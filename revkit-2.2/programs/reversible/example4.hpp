#ifndef HDR_H
#define HDR_H

#include <reversible/circuit.hpp>
#include <reversible/gate.hpp>
#include <reversible/target_tags.hpp>
#include <reversible/functions/reverse_circuit.hpp>
#include <reversible/functions/clear_circuit.hpp>
#include <reversible/functions/copy_circuit.hpp>
#include <reversible/io/read_realization.hpp>
#include <reversible/io/read_specification.hpp>
#include <reversible/io/write_specification.hpp>
#include <reversible/io/write_realization.hpp>
#include <reversible/io/print_circuit.hpp>
#include <reversible/synthesis/transformation_based_synthesis.hpp>
#include <reversible/synthesis/reed_muller_synthesis.hpp>
#include <reversible/synthesis/quantified_exact_synthesis.hpp>
#include <reversible/functions/circuit_to_truth_table.hpp>
#include <reversible/functions/extend_truth_table.hpp>
#include <reversible/simulation/simple_simulation.hpp>
#include <reversible/simulation/dalgo.hpp>
#include <reversible/functions/add_circuit.hpp>
#include <reversible/functions/add_gates.hpp>
#include <reversible/optimization/window_optimization.hpp>
#include <reversible/io/print_statistics.hpp>
namespace revkit
{
extern int bitsize;
extern int no_partialprod;
extern int no_of_inputlines;
extern circuit wallace;
extern void wallace_mult();
}
#endif
