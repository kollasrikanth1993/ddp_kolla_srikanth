/* RevKit (www.revkit.org)
 * Copyright (C) 2009-2014  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#include <iostream>

#include <reversible/xor.hpp>
#include <reversible/and.hpp>
#include <reversible/or.hpp>
#include <reversible/fa.hpp>
#include <reversible/simulation/simple_simulation.hpp>

using namespace revkit;

int zero_line = 0;
int one_line = 1;
int copy_line_start_no = 0;
int f_module_end_no = 0;
int next_module_start_no = 0;
int f_module_start_no;
int k_start = 2;
int m_start;
int lr_lines_start; 

int full_adder( circuit& circ1, int init_start, int no_of_bits, int x_start, int y_start);
int left_rotate_module(circuit& circ1, int bits_start, int no_of_bits, int iter);

int s[64] = {1, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
	4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23, 6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21};

	// Function to create circuit for (B AND C) OR (~B AND D)
	void part1( circuit& circ1, int init_start, int no_of_bits, int zero_line, int one_line){
		// Input lines of a, b, c, ~b and d
		//f_module_start_no = init_start;

		int a_start = f_module_start_no, b_start = a_start + no_of_bits, c_start = b_start + no_of_bits, d_start = c_start + no_of_bits, f_start = d_start + no_of_bits;
		//Copy Gate Location
		int copy_b_start = f_start + no_of_bits, copy_c_start = copy_b_start + no_of_bits, copy_d_start = copy_c_start + no_of_bits;

		// Input line for ~B
		int not_b_start = init_start + no_of_bits;

		// Aux input lines
		int bcaux_start = not_b_start + no_of_bits, notbdaux_start = bcaux_start + no_of_bits, oraux_start = notbdaux_start + no_of_bits;
		
		// Building copy gates and sending them as input to feedback module
		
		copy_line_start_no = copy_b_start;
		f_module_end_no = f_start + no_of_bits;
		 
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( b_start + i) (copy_b_start + i);
		}

		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( c_start + i) (copy_c_start + i);
		}

		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( d_start + i) (copy_d_start + i);
		}

		//---------------------------------------------------------------------------
		// For calculating ~B
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( b_start + i) (not_b_start + i);
		}
		
		
		// For calculating (B AND C)
		for(int i = 0; i < no_of_bits; i++){
			//std::cout << bcaux_start + i << " " << b_start + i << " " << c_start + i << std::endl;
			append_AND( circ1, bcaux_start + i, b_start + i, c_start + i, one_line);	// All aux inputs have to be driven to 0, enable is 0
		}

		// Circuit for (~B AND D)
		for(int i = 0; i < no_of_bits; i++){
			append_AND( circ1, notbdaux_start + i, not_b_start + i, d_start + i, one_line);
		}

		// Circuit for (B AND C) OR (~B AND D)
		for(int i = 0; i < no_of_bits; i++){
			append_OR( circ1, oraux_start + i, c_start + i, d_start + i);
		}
	
		// Output lines will be d_start + i
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1) (d_start + i)( f_start + i);
		}

		//std::cout << circ1 << std::endl;
	
	}
	
	// Function to create circuit for (D AND B) OR (~D AND C)
	void part2( circuit& circ1, int init_start, int no_of_bits, int zero_line, int one_line){
		// Input lines for A, B, C, D and ~D
		//f_module_start_no = init_start;
		int a_start = f_module_start_no, b_start = a_start + no_of_bits, c_start = b_start + no_of_bits, d_start = c_start + no_of_bits, f_start = d_start + no_of_bits;;

		//Copy Gate Location
		int copy_b_start = f_start + no_of_bits, copy_c_start = copy_b_start + no_of_bits, copy_d_start = copy_c_start + no_of_bits;

		int not_d_start = init_start + no_of_bits;

		// Input for aux lines
		int bdaux_start = not_d_start + no_of_bits, notdcaux_start = bdaux_start + no_of_bits, oraux_start = notdcaux_start + no_of_bits;
		
		// Building copy gates and sending them as input to feedback module

		copy_line_start_no = copy_b_start;
		f_module_end_no = f_start + no_of_bits;
		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( b_start + i) (copy_b_start + i);
		}

		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( c_start + i) (copy_c_start + i);
		}

		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( d_start + i) (copy_d_start + i);
		}

		//---------------------------------------------------------------------------
		

		// Circuit for Negating D (~D) -- output into not_d_start lines
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( d_start + i) (not_d_start + i);
		}

		// For calculating (D AND B)
		for(int i = 0; i < no_of_bits; i++){
			append_AND( circ1, bdaux_start + i, d_start + i, b_start + i, one_line);	// All aux inputs have to be driven to 0, enable is 0
		}

		// Circuit for (~D AND C)
		for(int i = 0; i < no_of_bits; i++){
			append_AND( circ1, notdcaux_start + i, not_d_start + i, c_start + i, one_line);
		}

		// Circuit for (D AND B) OR (~D AND C)
		for(int i = 0; i < no_of_bits; i++){
			append_OR( circ1, oraux_start + i, b_start + i, c_start + i);
		}

		// Output lines will be c_start + i

		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1)( c_start + i)(f_start + i);
		}


		//std::cout << circ1 << std::endl;
	}

	// Function to create circuit for (B XOR C XOR D)
	void part3( circuit& circ1, int init_start, int no_of_bits, int zero_line, int one_line){
		// Input lines for A, B, C and D
		//f_module_start_no = init_start;
		int a_start = f_module_start_no, b_start = a_start + no_of_bits, c_start = b_start + no_of_bits, d_start = c_start + no_of_bits, f_start = d_start + no_of_bits;;

		//Copy Gate Location
		int copy_b_start = f_start + no_of_bits, copy_c_start = copy_b_start + no_of_bits, copy_d_start = copy_c_start + no_of_bits;
		
		//Input for aux lines
		int bcaux_start = init_start + no_of_bits, bcdaux_start = bcaux_start + no_of_bits;

		// Building copy gates and sending them as input to feedback module

		copy_line_start_no = copy_b_start;
		f_module_end_no = f_start + no_of_bits;
		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( b_start + i) (copy_b_start + i);
		}

		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( c_start + i) (copy_c_start + i);
		}

		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( d_start + i) (copy_d_start + i);
		}

		//---------------------------------------------------------------------------


		// For calculating (B XOR C)
		for(int i = 0; i < no_of_bits; i++){
			append_XOR( circ1, bcaux_start + i, b_start + i, c_start + i);	// All aux inputs have to be driven to 0, enable is 0
		}

		// Circuit for (B XOR C XOR D)
		for(int i = 0; i < no_of_bits; i++){
			append_XOR( circ1, bcdaux_start + i, c_start + i, d_start + i);
		}

		// Output lines will be d_start + i

		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1)( d_start + i)( f_start + i);
		}


		std::cout << circ1 << std::endl;
	}
	
	
	// Function to create circuit for C XOR (B OR ~D)
	void part4( circuit& circ1, int init_start, int no_of_bits, int zero_line, int one_line){
		// Input for lines A, B, C, D and ~D
		//f_module_start_no = init_start;
		int a_start = f_module_start_no, b_start = a_start + no_of_bits, c_start = b_start + no_of_bits, d_start = c_start + no_of_bits, f_start = d_start + no_of_bits;
		
		int copy_b_start = f_start + no_of_bits, copy_c_start = copy_b_start + no_of_bits, copy_d_start = copy_c_start + no_of_bits;

		int not_d_start = init_start + no_of_bits;
		// Input for aux lines
		int bdaux_start = not_d_start + no_of_bits, cbdaux_start = bdaux_start + no_of_bits;

		// Building copy gates and sending them as input to feedback module

		copy_line_start_no = copy_b_start;
		f_module_end_no = f_start + no_of_bits;
		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( b_start + i) (copy_b_start + i);
		}
		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( c_start + i) (copy_c_start + i);
		}

		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( d_start + i) (copy_d_start + i);
		}

		//---------------------------------------------------------------------------


		
		// Circuit for Negating D (~D) -- output into not_d_start lines
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 )( d_start + i )( not_d_start + i );
		}

		
		// For calculating (B OR ~D)
		for(int i = 0; i < no_of_bits; i++){
			append_OR( circ1, bdaux_start + i, b_start + i, not_d_start + i);	// All aux inputs have to be driven to 0, enable is 0
		}
	
		// Circuit for C XOR (B OR ~D)
		for(int i = 0; i < no_of_bits; i++){
			append_XOR( circ1, cbdaux_start + i, not_d_start + i, c_start + i);
		}

		// Output lines will be c_start + i

		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1)( c_start + i)( f_start + i);
		}


		//std::cout << circ1 << std::endl;
	
	}

	void feedback_module( circuit& circ1, int no_of_bits, int zero_line, int one_line, int iter){
		// Feedback lines for A, B, C and D
		int d_Temp = copy_line_start_no + 3 * no_of_bits, fa1_aux = d_Temp + no_of_bits, fa2_aux = fa1_aux + no_of_bits + 1, fa3_aux = fa2_aux + no_of_bits + 1, fa4_aux = fa3_aux + no_of_bits + 1; 
		

		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 ) (copy_line_start_no + 2 * (no_of_bits) + i) ( d_Temp + i);
		}
		
		// Feedback for D
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 ) (copy_line_start_no + (no_of_bits) + i) ( f_module_start_no + 3 * (no_of_bits) + i);
		}

		// Feedback for C
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 ) (copy_line_start_no + i) ( f_module_start_no + 2 * (no_of_bits) + i);
		}

		// Feedback for B -- Need to do left rotate
		
		// A + F + K[i] + M[g]
		int y1_start = full_adder(circ1, fa1_aux, no_of_bits, f_module_start_no, f_module_end_no - no_of_bits);
		int y2_start = full_adder(circ1, fa2_aux, no_of_bits, k_start, y1_start);
		int y3_start = full_adder(circ1, fa3_aux, no_of_bits, y2_start, m_start);

		// Now, we have to perform left rotate on y3_start
		
		int afterlr_start = left_rotate_module(circ1, y3_start, no_of_bits, iter);
		
		// Adding the left rotated value to B
		int y4_start = full_adder(circ1, fa4_aux, no_of_bits, copy_line_start_no, afterlr_start);

		// Feeding back the result to B
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 ) (copy_line_start_no + i) ( f_module_start_no + no_of_bits + i);
		}
		
		// ----------------------------------------------------------

		//Feedback for A
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 ) (d_Temp + i) ( f_module_start_no + i);
		}
		
		f_module_end_no = fa4_aux + no_of_bits + 1;

		// Killing the copy gate lines

		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 ) ( zero_line) ( copy_line_start_no + i);
		}
		
		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 ) ( zero_line) ( copy_line_start_no + no_of_bits + i);
		}

		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 ) ( zero_line) ( copy_line_start_no + 2 * (no_of_bits) + i);
		}

		for(int i = 0; i < no_of_bits; i++){
			append_toffoli( circ1 ) ( zero_line) ( d_Temp);
		}

	}


	int full_adder( circuit& circ1, int init_start, int no_of_bits, int x_start, int y_start){
		int fa_aux = init_start + no_of_bits, carry_bit = fa_aux + 1;
		int bitsize = no_of_bits;
		for(int i = no_of_bits - 1; i >= 0; i--){
			append_FA( circ1, fa_aux + i, carry_bit , x_start + i, y_start + i);
		}
		return fa_aux;
	}

	int left_rotate_module(circuit& circ1, int bits_start, int no_of_bits, int iter){
		for(int i = 0; i < s[iter]; i++){
			append_toffoli (circ1 ) ( bits_start ) ( lr_lines_start + no_of_bits - 1 - i);
			for(int j = 1; j < no_of_bits; j++){
				append_toffoli( circ1 )(zero_line) (bits_start + j - 1);
				append_toffoli( circ1 )( bits_start + j) ( bits_start + j - 1);
			}
			bits_start = lr_lines_start + no_of_bits - 1 - i;
		}
		std::cout << circ1 << std::endl;
		return bits_start;
	}

int main( int argc, char ** argv )
{	
	int no_of_bits = 3;
	m_start = k_start + no_of_bits;
	lr_lines_start = m_start + no_of_bits;
	f_module_start_no = lr_lines_start + no_of_bits;
	int no_inputlines = 15;
	int init_start = f_module_start_no + 5 * no_of_bits;
	circuit circ1(no_inputlines);
	//part1(circ1, init_start, no_of_bits, zero_line, one_line);
	//part2(circ1, init_start, no_of_bits, zero_line, one_line);
	//part3(circ1, init_start, no_of_bits, zero_line, one_line);
	//part4(circ1, init_start, no_of_bits, zero_line, one_line);

	left_rotate_module(circ1, 11, no_of_bits, 0);
	/*for(int i = 0; i < 63; i++){
		if( i < 15){
			part1(circ1, f_module_end_no, no_of_bits, zero_line, one_line);
			feedback_module(circ1, no_of_bits, zero_line, one_line, i);	
		}
		else if( i >= 15 && i < 30){
			part2(circ1, f_module_end_no, no_of_bits, zero_line, one_line);
			feedback_module(circ1, no_of_bits, zero_line, one_line, (5*i + 1) % 16);
		}
		else if( i >= 30 && i < 45){
			part3(circ1, f_module_end_no, no_of_bits, zero_line, one_line);
			feedback_module(circ1, no_of_bits, zero_line, one_line, (3*i + 5) % 16);
		}
		else{
			part4(circ1, f_module_end_no, no_of_bits, zero_line, one_line);
			feedback_module(circ1, no_of_bits, zero_line, one_line, (7*i) % 16);
		}		
	}*/
	
	// Simulating the circuit (For testing purpose)

	boost::dynamic_bitset<> out;
	boost::dynamic_bitset<> in(15);

	//Default for part 1 & part 2 & part 4
	//in[1] = 1;
	//in[8] = 1;
	//in[9] = 1;
	//in[10] = 1;

	// Custom input here
	in[11] = 1;
	in[12] = 1;
	in[13] = 1;
	simple_simulation(out,circ1,in);
	
	std::cout<<"\nInput: ";
	for(size_t i = 0; i < no_inputlines; i++)
	{
		std::cout<<in[i]<<" ";
	}

	std::cout<<"\noutput: ";
	for(size_t i = 0; i < no_inputlines; i++)
	{
		std::cout<<out[i]<<" ";
	}


  	return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
