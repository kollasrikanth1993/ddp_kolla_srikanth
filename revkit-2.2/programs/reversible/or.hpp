#include <iostream>

#include <reversible/circuit.hpp>
#include <reversible/functions/add_gates.hpp>
#include <reversible/io/print_circuit.hpp>


namespace revkit{

	circuit& add_OR(circuit& circ){
		append_toffoli( circ )( 0u )( 1 );
		append_toffoli( circ )( 1u )( 0 );
		append_toffoli( circ )( 0u, 1u )( 2 );
		append_toffoli( circ )( 0u, 2u )( 1 );
		append_toffoli( circ )( 1u, 2u )( 0 );
		append_toffoli( circ )( 0u, 1u )( 2 );
		append_toffoli( circ )( 2u )( 0 );
		//std::cout << circ << std::endl;
		return circ;
	}
}


