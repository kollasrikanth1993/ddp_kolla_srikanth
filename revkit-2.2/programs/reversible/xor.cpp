/* RevKit (www.revkit.org)
 * Copyright (C) 2009-2014  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#include <iostream>

#include <reversible/xor.hpp>

#include <reversible/simulation/simple_simulation.hpp>

using namespace revkit;

int main( int argc, char ** argv )
{

  circuit circ1 ( 3 );
  append_XOR( circ1, 0 , 1, 2 );

  std::cout << circ1 << std::endl;

boost::dynamic_bitset<> out;
  boost::dynamic_bitset<> in(3,0ul);
  simple_simulation(out,circ1,in);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<in[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<out[i]<<" ";
  }

boost::dynamic_bitset<> in1(3,1ul);
  simple_simulation(out,circ1,in1);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<in1[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<out[i]<<" ";
  }

boost::dynamic_bitset<> in2(3,2ul);
  simple_simulation(out,circ1,in2);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<in2[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<out[i]<<" ";
  }

boost::dynamic_bitset<> in3(3,3ul);
  simple_simulation(out,circ1,in3);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<in3[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<out[i]<<" ";
  }

boost::dynamic_bitset<> in4(3,4ul);
  simple_simulation(out,circ1,in4);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<in4[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<out[i]<<" ";
  }

boost::dynamic_bitset<> in5(3,5ul);
  simple_simulation(out,circ1,in5);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<in5[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<out[i]<<" ";
  }

boost::dynamic_bitset<> in6(3,6ul);
  simple_simulation(out,circ1,in6);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<in6[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<out[i]<<" ";
  }

boost::dynamic_bitset<> in7(3,7ul);
  simple_simulation(out,circ1,in7);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<in7[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<out[i]<<" ";
  }


  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
