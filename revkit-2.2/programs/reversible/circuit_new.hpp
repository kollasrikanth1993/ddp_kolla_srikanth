#include <sstream>
#include <bitset>
#include <string>
#include <boost/algorithm/string.hpp>

#include <reversible/xor.hpp>
#include <reversible/and.hpp>
#include <reversible/or.hpp>
#include <reversible/fa.hpp>
#include <reversible/simulation/simple_simulation.hpp>

namespace revkit{
	extern int no_inputlines;
	extern circuit md5_circ;
	extern int no_of_bits;
	extern int k_start;
	extern int init_start;

	extern int zero_line;
	extern int one_line;
	extern int m_start;
	extern int init_start;

	extern int copy_line_start_no;
	extern int f_val_lines;
	extern int f_module_start_no;
	extern int lr_lines_start;
	extern int round_end_no;
	extern int f_end_no;
	
	extern std::vector<std::string> k;
	using namespace std;
	//extern string Hex2bin(string&);
	extern void md5_implementation();
}
