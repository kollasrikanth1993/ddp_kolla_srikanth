/* RevKit (www.revkit.org)
 * Copyright (C) 2009-2014  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#include <iostream>

#include "circuit_new.hpp"

using namespace revkit;


namespace revkit{
int no_inputlines = 4106;
circuit md5_circ(no_inputlines);
int no_of_bits = 32;

int zero_line = 0;
int one_line = 1;
int m_start = 2;
int k_start = m_start + 16 * no_of_bits;
int tempA = k_start + 64 * no_of_bits;
int tempB = tempA + no_of_bits;
int tempC = tempB + no_of_bits;
int tempD = tempC + no_of_bits;
int init_start = tempD + no_of_bits;

int copy_line_start_no;
int f_val_lines;
int f_module_start_no;
int lr_lines_start;
int round_end_no = init_start, f_end_no = 0;


using namespace std;
std::vector<std::string> k = {"0xd76aa478", "0xe8c7b756", "0x242070db", "0xc1bdceee", "0xf57c0faf", "0x4787c62a", "0xa8304613", "0xfd469501", "0x698098d8", "0x8b44f7af", "0xffff5bb1", "0x895cd7be", "0x6b901122", "0xfd987193", "0xa679438e", "0x49b40821", "0xf61e2562", "0xc040b340", "0x265e5a51", "0xe9b6c7aa", "0xd62f105d", "0x02441453", "0xd8a1e681", "0xe7d3fbc8", "0x21e1cde6", "0xc33707d6", "0xf4d50d87", "0x455a14ed", "0xa9e3e905", "0xfcefa3f8", "0x676f02d9", "0x8d2a4c8a", "0xfffa3942", "0x8771f681", "0x6d9d6122", "0xfde5380c", "0xa4beea44", "0x4bdecfa9", "0xf6bb4b60", "0xbebfbc70", "0x289b7ec6", "0xeaa127fa", "0xd4ef3085", "0x04881d05", "0xd9d4d039", "0xe6db99e5", "0x1fa27cf8", "0xc4ac5665", "0xf4292244", "0x432aff97", "0xab9423a7", "0xfc93a039", "0x655b59c3", "0x8f0ccc92", "0xffeff47d", "0x85845dd1", "0x6fa87e4f", "0xfe2ce6e0", "0xa3014314", "0x4e0811a1", "0xf7537e82", "0xbd3af235", "0x2ad7d2bb", "0xeb86d391"};

void md5_implementation(void);
string Hex2Bin(string&);

}

int full_adder( circuit& md5_circ, int init_start, int no_of_bits, int x_start, int y_start);
int left_rotate_module(circuit& md5_circ, int bits_start, int lr_lines_start, int no_of_bits, int iter);

int s[64] = {7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
	4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23, 6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21};

// Function to create circuit for (B AND C) OR (~B AND D)
int part1( circuit& md5_circ, int init_start, int no_of_bits, int zero_line, int one_line){
	// Input lines of a, b, c, ~b and d
	f_module_start_no = init_start;

	int a_start = init_start, b_start = a_start + no_of_bits, c_start = b_start + no_of_bits, d_start = c_start + no_of_bits, f_start = d_start + no_of_bits;
	//Copy Gate Location
	int copy_b_start = f_start + no_of_bits, copy_c_start = copy_b_start + no_of_bits, copy_d_start = copy_c_start + no_of_bits;

	// Input line for ~B
	int not_b_start = copy_d_start + no_of_bits;

	// Aux input lines
	int bcaux_start = not_b_start + no_of_bits, notbdaux_start = bcaux_start + no_of_bits, oraux_start = notbdaux_start + no_of_bits;
	

	
	int f_module_end_no = oraux_start + no_of_bits;
	// Building copy gates and sending them as input to feedback module
	
	copy_line_start_no = copy_b_start;
	f_val_lines = f_start;
	 
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( b_start + i) (copy_b_start + i);
	}

	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( c_start + i) (copy_c_start + i);
	}

	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( d_start + i) (copy_d_start + i);
	}


	//---------------------------------------------------------------------------
	// For calculating ~B
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( b_start + i) (not_b_start + i);
	}
	
	
	// For calculating (B AND C)
	for(int i = 0; i < no_of_bits; i++){
		//std::cout << bcaux_start + i << " " << b_start + i << " " << c_start + i << std::endl;
		append_AND( md5_circ, bcaux_start + i, b_start + i, c_start + i, one_line);	
		// All aux inputs have to be driven to 0, enable is 0
	}

	// Circuit for (~B AND D)
	for(int i = 0; i < no_of_bits; i++){
		append_AND( md5_circ, notbdaux_start + i, not_b_start + i, d_start + i, one_line);
	}
	
	// Circuit for (B AND C) OR (~B AND D)
	for(int i = 0; i < no_of_bits; i++){
		append_OR( md5_circ, oraux_start + i, c_start + i, d_start + i);
	}
	
	// Output lines will be d_start + i
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ) (d_start + i)( f_start + i);
	}
	
	//std::cout << md5_circ << std::endl;
	return f_module_end_no;
}
	
// Function to create circuit for (D AND B) OR (~D AND C)
int part2( circuit& md5_circ, int init_start, int no_of_bits, int zero_line, int one_line){
	// Input lines for A, B, C, D and ~D
	f_module_start_no = init_start;
	int a_start = init_start, b_start = a_start + no_of_bits, c_start = b_start + no_of_bits, d_start = c_start + no_of_bits, f_start = d_start + no_of_bits;

	//Copy Gate Location
	int copy_b_start = f_start + no_of_bits, copy_c_start = copy_b_start + no_of_bits, copy_d_start = copy_c_start + no_of_bits;

	int not_d_start = copy_d_start + no_of_bits;

	// Input for aux lines
	int bdaux_start = not_d_start + no_of_bits, notdcaux_start = bdaux_start + no_of_bits, oraux_start = notdcaux_start + no_of_bits;
	

	int f_module_end_no = oraux_start + no_of_bits;
	// Building copy gates and sending them as input to feedback module

	copy_line_start_no = copy_b_start;
	f_val_lines = f_start;
	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( b_start + i) (copy_b_start + i);
	}

	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( c_start + i) (copy_c_start + i);
	}

	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( d_start + i) (copy_d_start + i);
	}

	//---------------------------------------------------------------------------
	

	// Circuit for Negating D (~D) -- output into not_d_start lines
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( d_start + i) (not_d_start + i);
	}

	// For calculating (D AND B)
	for(int i = 0; i < no_of_bits; i++){
		append_AND( md5_circ, bdaux_start + i, d_start + i, b_start + i, one_line);	// All aux inputs have to be driven to 0, enable is 0
	}

	// Circuit for (~D AND C)
	for(int i = 0; i < no_of_bits; i++){
		append_AND( md5_circ, notdcaux_start + i, not_d_start + i, c_start + i, one_line);
	}

	// Circuit for (D AND B) OR (~D AND C)
	for(int i = 0; i < no_of_bits; i++){
		append_OR( md5_circ, oraux_start + i, b_start + i, c_start + i);
	}

	// Output lines will be c_start + i

	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ)( c_start + i)(f_start + i);
	}

	return f_module_end_no;
	//std::cout << md5_circ << std::endl;
}

// Function to create circuit for (B XOR C XOR D)
int part3( circuit& md5_circ, int init_start, int no_of_bits, int zero_line, int one_line){
	// Input lines for A, B, C and D
	f_module_start_no = init_start;
	int a_start = init_start, b_start = a_start + no_of_bits, c_start = b_start + no_of_bits, d_start = c_start + no_of_bits, 			f_start = d_start + no_of_bits;

	//Copy Gate Location
	int copy_b_start = f_start + no_of_bits, copy_c_start = copy_b_start + no_of_bits, copy_d_start = copy_c_start + 			no_of_bits;
	
	//Input for aux lines
	int bcaux_start = copy_d_start + no_of_bits, bcdaux_start = bcaux_start + no_of_bits;


	int f_module_end_no = bcdaux_start + no_of_bits;
	// Building copy gates and sending them as input to feedback module

	copy_line_start_no = copy_b_start;
	f_val_lines = f_start;
	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( b_start + i) (copy_b_start + i);
	}

	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( c_start + i) (copy_c_start + i);
	}

	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( d_start + i) (copy_d_start + i);
	}

	//---------------------------------------------------------------------------


	// For calculating (B XOR C)
	for(int i = 0; i < no_of_bits; i++){
		append_XOR( md5_circ, bcaux_start + i, b_start + i, c_start + i);	// All aux inputs have to be driven to 0, enable is 0
	}

	// Circuit for (B XOR C XOR D)
	for(int i = 0; i < no_of_bits; i++){
		append_XOR( md5_circ, bcdaux_start + i, c_start + i, d_start + i);
	}

	// Output lines will be d_start + i

	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ)( d_start + i)( f_start + i);
	}


	//std::cout << md5_circ << std::endl;
	return f_module_end_no;
}

	
// Function to create circuit for C XOR (B OR ~D)
int part4( circuit& md5_circ, int init_start, int no_of_bits, int zero_line, int one_line){
	// Input for lines A, B, C, D and ~D
	f_module_start_no = init_start;
	int a_start = init_start, b_start = a_start + no_of_bits, c_start = b_start + no_of_bits, d_start = c_start + no_of_bits, 			f_start = d_start + no_of_bits;
	
	int copy_b_start = f_start + no_of_bits, copy_c_start = copy_b_start + no_of_bits, copy_d_start = copy_c_start + 			no_of_bits;

	int not_d_start = copy_d_start + no_of_bits;
	// Input for aux lines
	int bdaux_start = not_d_start + no_of_bits, cbdaux_start = bdaux_start + no_of_bits;

	int f_module_end_no = cbdaux_start + no_of_bits;
	// Building copy gates and sending them as input to feedback module

	copy_line_start_no = copy_b_start;
	f_val_lines = f_start;
	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( b_start + i) (copy_b_start + i);
	}
	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( c_start + i) (copy_c_start + i);
	}

	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( d_start + i) (copy_d_start + i);
	}

	//---------------------------------------------------------------------------


	
	// Circuit for Negating D (~D) -- output into not_d_start lines
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ )( d_start + i )( not_d_start + i );
	}

	
	// For calculating (B OR ~D)
	for(int i = 0; i < no_of_bits; i++){
		append_OR( md5_circ, bdaux_start + i, b_start + i, not_d_start + i);	// All aux inputs have to be driven to 0, enable is 0
	}

	// Circuit for C XOR (B OR ~D)
	for(int i = 0; i < no_of_bits; i++){
		append_XOR( md5_circ, cbdaux_start + i, not_d_start + i, c_start + i);
	}

	// Output lines will be c_start + i

	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ)( c_start + i)( f_start + i);
	}


	//std::cout << md5_circ << std::endl;
	return f_module_end_no;
}

int feedback_module( circuit& md5_circ, int init_start, int no_of_bits, int zero_line, int one_line, int iter){
	// Feedback lines for A, B, C and D
	int d_Temp = init_start, fa1_aux = d_Temp + no_of_bits, fa2_aux = fa1_aux + no_of_bits + 1, fa3_aux = fa2_aux + no_of_bits 		+ 1, fa4_aux = fa3_aux + no_of_bits + 1, lr_lines_start = fa4_aux + no_of_bits + 1, copy_m_start = lr_lines_start + no_of_bits, copy_k_start = copy_m_start + no_of_bits;
	
	int feedback_end_no = copy_k_start + no_of_bits;
	//std::cout<<feedback_end_no<<std::endl;
	
	// Copying the message value from original lines

	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ ) (m_start + iter * (no_of_bits) + i) ( copy_m_start + i);
	}
	
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ ) (k_start + iter * no_of_bits + i) ( copy_k_start + i);
	}
	
	// Copying D into dTemp
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ ) (copy_line_start_no + 2 * (no_of_bits) + i) ( d_Temp + i);
	}
	
	// Feedback for D
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ ) (copy_line_start_no + (no_of_bits) + i) ( feedback_end_no + 3 * (no_of_bits) + i);
	}

	// Feedback for C
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ ) ( copy_line_start_no + i) ( feedback_end_no + 2 * (no_of_bits) + i);
	}

	// Feedback for B -- Need to do left rotate
	
	// A + F + K[i] + M[g]
	int y1_start = full_adder(md5_circ, fa1_aux, no_of_bits, f_module_start_no, f_val_lines);
	int y2_start = full_adder(md5_circ, fa2_aux, no_of_bits, copy_k_start, y1_start);
	int y3_start = full_adder(md5_circ, fa3_aux, no_of_bits, y2_start, copy_m_start);

	// Now, we have to perform left rotate on y3_start
	
	int afterlr_start = left_rotate_module(md5_circ, y3_start, lr_lines_start, no_of_bits, iter);
	
	// Adding the left rotated value to B
	int y4_start = full_adder(md5_circ, fa4_aux, no_of_bits, copy_line_start_no, afterlr_start);
	
	// Feeding back the result to B
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ ) (y4_start + i) ( feedback_end_no + no_of_bits + i);
	}
	
	// ----------------------------------------------------------

	//Feedback for A
	for(int i = 0; i < no_of_bits; i++){
		append_toffoli( md5_circ ) (d_Temp + i) ( feedback_end_no + i);
	}
	
	return feedback_end_no;
}


int full_adder( circuit& md5_circ, int init_start, int no_of_bits, int x_start, int y_start){
	int fa_aux = init_start, carry_bit = fa_aux + no_of_bits;
	int bitsize = no_of_bits;
	for(int i = no_of_bits - 1; i >= 0; i--){
		append_FA( md5_circ, fa_aux + i, carry_bit , x_start + i, y_start + i);
	}
	return fa_aux;
}

int left_rotate_module(circuit& md5_circ, int bits_start, int lr_lines_start, int no_of_bits, int iter){
	int init_copy = s[iter];
	for(int i = init_copy; i < no_of_bits; i++){
		append_toffoli (md5_circ ) ( bits_start + i ) ( lr_lines_start + i - init_copy);
	}

	for(int i = init_copy -1; i >= 0; i--){
		append_toffoli (md5_circ ) ( bits_start + i) ( lr_lines_start + no_of_bits - init_copy + i);
	}
	//std::cout << md5_circ << std::endl;
	return lr_lines_start;
}

int final_append_module(circuit& md5_circ, int init_start){
	int fa1_aux = init_start, fa2_aux = fa1_aux + no_of_bits + 1, fa3_aux = fa2_aux + no_of_bits + 1,fa4_aux = fa3_aux + no_of_bits+ 1;
	int A = fa4_aux + no_of_bits + 1;
	int B = A + no_of_bits;
	int C = B + no_of_bits;
	int D = C + no_of_bits;

	int a_start = full_adder(md5_circ, fa1_aux, no_of_bits, tempA, round_end_no);
	int b_start = full_adder(md5_circ, fa2_aux, no_of_bits, tempB, round_end_no + no_of_bits);
	int c_start = full_adder(md5_circ, fa3_aux, no_of_bits, tempC, round_end_no + 2 * no_of_bits);
	int d_start = full_adder(md5_circ, fa4_aux, no_of_bits, tempD, round_end_no + 3 * no_of_bits);

	for(int i = 0; i < no_of_bits; i++){
		append_toffoli(md5_circ) (a_start + i) (A + i);
	}

	for(int i = 0; i < no_of_bits; i++){
		append_toffoli(md5_circ) (b_start + i) (B + i);
	}

	for(int i = 0; i < no_of_bits; i++){
		append_toffoli(md5_circ) (c_start + i) (C + i);
	}

	for(int i = 0; i < no_of_bits; i++){
		append_toffoli(md5_circ) (d_start + i) (D + i);
	}
}

void revkit::md5_implementation(){
	/*	
	for(int i = 0; i < 64; i++){
		//cout<< "f_end_no: " << f_end_no << endl;
		//cout<< "round_end_no: " << round_end_no << endl;
		if( i <= 15){
			f_end_no = part1(md5_circ, round_end_no, no_of_bits, zero_line, one_line);
			round_end_no = feedback_module(md5_circ, f_end_no, no_of_bits, zero_line, one_line, i);	
		}
		else if( i > 15 && i <= 31){
			f_end_no = part2(md5_circ, round_end_no, no_of_bits, zero_line, one_line);
			round_end_no = feedback_module(md5_circ, f_end_no, no_of_bits, zero_line, one_line, (5*i + 1) % 16);
		}
		else if( i > 31 && i <= 47){
			f_end_no = part3(md5_circ, round_end_no, no_of_bits, zero_line, one_line);
			round_end_no = feedback_module(md5_circ, f_end_no, no_of_bits, zero_line, one_line, (3*i + 5) % 16);
		}
		else{
			f_end_no = part4(md5_circ, round_end_no, no_of_bits, zero_line, one_line);
			round_end_no = feedback_module(md5_circ, f_end_no, no_of_bits, zero_line, one_line, (7*i) % 16);
		}		
	}
	
	final_append_module(md5_circ, round_end_no + 4 * (no_of_bits));
	*/
	//cout << " final f_end_no: " << f_end_no << endl;
	//cout<< "round_end_no: " << round_end_no << endl;
}


using namespace std;
string Hex2bin(string& s)
{
    const unsigned g_unMaxBits = 32;
    stringstream ss;
    ss << hex << s;
    unsigned n;
    ss >> n;
    bitset<g_unMaxBits> b(n);

    unsigned x = 0;
    if (boost::starts_with(s, "0x") || boost::starts_with(s, "0X")) x = 2;
    return b.to_string().substr(32 - 4*(s.length()-x));
}


int main( int argc, char ** argv )
{       
	 boost::dynamic_bitset<> outp;
	 boost::dynamic_bitset<> inp(no_inputlines);

	int num = 0;

	// Initialising zero line and one line
	inp[0] = 0;
	inp[1] = 1;

	//Initialising m
	string m_0 = "0x79433973";
	string m = Hex2bin(m_0);
	for(int j = 0; j < no_of_bits; j++){
		inp[m_start + j] = (int) (m.at(j) - '0');
	}

	string m_1 = "0xf9d6a0e7";
	string m1 = Hex2bin(m_1);
	for(int j = 0; j < no_of_bits; j++){
		inp[m_start + no_of_bits + j] = (int) (m1.at(j) - '0');
	}

	string m_2 = "0x158e7084";
	string m2 = Hex2bin(m_2);
	for(int j = 0; j < no_of_bits; j++){
		inp[m_start + 2 * no_of_bits + j] = (int) (m2.at(j) - '0');
	}

	string m_3 = "0x83074b71";
	string m3 = Hex2bin(m_3);
	for(int j = 0; j < no_of_bits; j++){
		inp[m_start + 3 * no_of_bits + j] = (int) (m3.at(j) - '0');
	}

	string m_4 = "0x6c6c6f6b";
	string m4 = Hex2bin(m_4);
	for(int j = 0; j < no_of_bits; j++){
		inp[m_start + 4 * no_of_bits + j] = (int) (m4.at(j) - '0');
	}

	string m_5 = "0x00008061";
	string m5 = Hex2bin(m_5);
	for(int j = 0; j < no_of_bits; j++){
		inp[m_start + 5 * no_of_bits + j] = (int) (m5.at(j) - '0');
	}
	
	string m_14 = "0x000000a8";
	string m14 = Hex2bin(m_14);
	for(int j = 0; j < no_of_bits; j++){
		inp[m_start + 14 * no_of_bits + j] = (int) (m14.at(j) - '0');
	}

	// Feeding the constant k[64] values into the input lines
	for(int i = k_start; i < k_start + 64 * no_of_bits; i += no_of_bits){
		string input = k.at(num);
		num++;
		string s = Hex2bin(input);
		for(int j = 0; j < no_of_bits; j++){
			inp[i + j] = (int) (s.at(j) - '0');
		}
	}

	// Initialising tempA
	string tempa_init = "0x67452301";
	string tempa = Hex2bin(tempa_init);
	for(int j = 0; j < no_of_bits; j++){
		inp[tempA + j] = (int) (tempa.at(j) - '0');
	}

	// Initialising tempB
	string tb_init = "0xefcdab89";
	string tb = Hex2bin(tb_init);
	for(int j = 0; j < no_of_bits; j++){
		inp[tempB + j] = (int) (tb.at(j) - '0');
	}

	// Initialising tempC
	string tc_init = "0x98badcfe";
	string tc = Hex2bin(tc_init);
	for(int j = 0; j < no_of_bits; j++){
		inp[tempC + j] = (int) (tc.at(j) - '0');
	}

	// Initialising tempD
	string td_init = "0x10325476";
	string td = Hex2bin(td_init);
	for(int j = 0; j < no_of_bits; j++){
		inp[tempD + j] = (int) (td.at(j) - '0');
	}

	// Initialising A
	string a_init = "0x67452301";
	string a = Hex2bin(a_init);
	std::cout<< "\nInit A value:";
	for(int j = 0; j < no_of_bits; j++){
		inp[init_start + j] = (int) (a.at(j) - '0');
		std::cout<<inp[init_start + j]<<" ";
	}

	// Initialising B
	string b_init = "0xefcdab89";
	string b = Hex2bin(b_init);
	std::cout<< "\nInit B value:";
	for(int j = 0; j < no_of_bits; j++){
		inp[init_start + no_of_bits + j] = (int) (b.at(j) - '0');
		std::cout<<inp[init_start + no_of_bits + j]<<" ";
	}

	// Initialising C
	string c_init = "0x98badcfe";
	string c = Hex2bin(c_init);
	std::cout<< "\nInit C value:";
	for(int j = 0; j < no_of_bits; j++){
		inp[init_start + 2 * no_of_bits + j] = (int) (c.at(j) - '0');
		std:cout<<inp[init_start + 2 * no_of_bits + j] << " ";
	}

	// Initialising D
	string d_init = "0x10325476";
	string d = Hex2bin(d_init);
	std::cout<< "\nInit D value:";
	for(int j = 0; j < no_of_bits; j++){
		inp[init_start + 3 * no_of_bits + j] = (int) (d.at(j) - '0');
		std::cout<<inp[init_start + 3 * no_of_bits + j] << " ";
	}

	//Initialising ~B lines
	for(int j = 0; j < no_of_bits; j++){
		inp[init_start + 8 * no_of_bits + j] = 1;
	}

	for(int j = 0; j < no_of_bits; j++){
		inp[3334 + 8 * no_of_bits + j] = 1;
	}

	md5_implementation();
	//std::cout<<md5_circ<<endl;
	simple_simulation(outp,md5_circ,inp);
	


	std::cout<< "\nF value:";
	for(int j = 0; j < no_of_bits; j++){
		std::cout<<outp[3334 + 3 * no_of_bits + j]<<" ";
	}
/*
	std::cout<< "\n~B AND D value:";
	for(int j = 0; j < no_of_bits; j++){
		std::cout<<outp[init_start + 3* no_of_bits + j]<<" ";
	}

	std::cout<< "\nA value:";
	for(int j = 0; j < no_of_bits; j++){
		std::cout<<outp[init_start + j]<<" ";
	}
*/
	
	/*
	std::cout<<"\nInput:\n";
	for(size_t i = 0; i<inp.size();i++)
	{
		std::cout<<inp[i]<<" ";
	}
	std::cout<<" \n Output:\n";

	for(size_t i = 0; i<outp.size();i++)
	{
		std::cout<<outp[i]<<" ";
	}
	std::cout<<"\n";
	*/

	std::cout<<"\n \nAfter 1 round A:";
	for(size_t i = no_inputlines - 4 * 32 ; i < no_inputlines - 3 * 32;i++)
	{
		std::cout<<outp[i]<<" ";
	}
	
	std::cout<<"\nAfter 1 round B:";
	for(size_t i = no_inputlines - 3 * 32 ; i < no_inputlines - 2 * 32;i++)
	{
		std::cout<<outp[i]<<" ";
	}
	
	std::cout<<"\nAfter 1 round C:";
	for(size_t i = no_inputlines - 2 * 32 ; i < no_inputlines - 32;i++)
	{
		std::cout<<outp[i]<<" ";
	}

	std::cout<<"\nAfter 1 round D:";
	for(size_t i = no_inputlines - 32 ; i < no_inputlines;i++)
	{
		std::cout<<outp[i]<<" ";
	}
	std::cout<<"\n";

	

}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
