#include <iostream>

#include <reversible/circuit.hpp>
#include <reversible/or.hpp>
#include <reversible/and.hpp>
#include <reversible/fa.hpp>

#include <reversible/simulation/simple_simulation.hpp>

using namespace revkit;

int main( int argc, char ** argv )
{
  int no_of_bits = 3;
  circuit circ1( 10 );
  int x_start = 0, y_start = 3, fa_aux = 6, carry_bit = 9;
  for(int i = no_of_bits - 1; i >= 0; i--){
	append_FA( circ1, fa_aux + i, carry_bit , x_start + i, y_start + i);
  }
  //std::cout << circ1 << std::endl;

  boost::dynamic_bitset<> out;
  boost::dynamic_bitset<> in(10);
  in[2] = 1;
  in[1] = 1;
  in[5] = 1;
  simple_simulation(out,circ1,in);

   std::cout<<"\nInput: ";
  for(size_t i=0;i<3;i++)
  {
	std::cout<<in[i]<<" ";
  }
  std::cout<<" and ";
  for(size_t i=3;i<6;i++)
  {
	std::cout<<in[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=6;i<9;i++)
  {
	std::cout<<out[i]<<" ";
  }
}
