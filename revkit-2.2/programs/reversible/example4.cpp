/* RevKit (www.revkit.org)
 * Copyright (C) 2009-2014  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
  @author Mathias Soeken
 */
#define EXAMPLE_MODE 0
#include <iostream>

#include "example4.hpp"
using namespace revkit;

/*No Of Full Adders at the root level. Indx starts from 0*/
namespace revkit{

int bitsize=7;
int no_partialprod=4;
int no_of_inputlines = ((no_partialprod/3)*4*(bitsize+1) + no_partialprod%3*(bitsize+1))+1+8+18+(2*12);//1 - Constant Line; 8 - Feeding 0's; 18 - Replicating Sum and Carry bits for CLA
circuit wallace(no_of_inputlines);
void wallace_mult(void);
}


int pos(int n)
{
	return (bitsize*4 + n);
}

void print_output(boost::dynamic_bitset<> inp, int no_inp, char const *desc)
{
	std::cout<<std::endl<<desc<<std::endl;
	for(size_t i=0; i<no_inp; i++)
  	{
		std::cout<<inp[i]<<" ";
	}
	std::cout<<std::endl;
}

void add_xor(circuit &wal, int i1, int i2, int i3)
{
  append_cnot(wal, i2, i1); 
  append_cnot(wal, i3, i1);
}

void add_pkillgate(circuit &wal, int i0, int i1, int i2, int i3, int i4, int i5)
{
  append_toffoli(wal) (i1,i2,i3)(i4);
  append_toffoli(wal) (i1,i2,i3,i5)(i4);
  append_toffoli(wal) (i0,i1,i2,i3)(i4);
  append_toffoli(wal) (i1,i2,i3,i4)(i0);
  append_toffoli(wal) (i0,i2,i3,i4,i5)(i1);
  append_toffoli(wal) (i0,i1,i2,i3,i5)(i4);
  append_toffoli(wal) (i0,i1)(i5);
  
  append_toffoli(wal) (i0,i2,i3,i5)(i4);
  append_toffoli(wal) (i0,i2,i5)(i1);
  append_toffoli(wal) (i0,i3,i5)(i1);
  append_toffoli(wal) (i0,i5)(i1);
  append_toffoli(wal) (i0,i1)(i5);
  append_toffoli(wal) (i0)(i4);
  
  append_toffoli(wal) (i1,i2,i4,i5)(i0);
  append_toffoli(wal) (i1,i3,i4,i5)(i0);
  append_toffoli(wal) (i1,i4,i5)(i0);
  
  
  append_toffoli(wal) (i1)(i5);
  append_toffoli(wal) (i4,i2,i3)(i0);
  append_toffoli(wal) (i0,i2,i3,i4)(i1);
  append_toffoli(wal) (i5,i2,i3)(i4);
  append_toffoli(wal) (i4,i2,i5)(i0);
  append_toffoli(wal) (i0,i2,i4,i5)(i1);
  append_toffoli(wal) (i2,i4)(i0);
  append_toffoli(wal) (i5,i2)(i1);
  
  
  append_toffoli(wal) (i3,i4,i5)(i0);
  append_toffoli(wal) (i0,i3,i5,i4)(i1);
  append_toffoli(wal) (i3,i4)(i0);
  append_toffoli(wal) (i3,i5)(i1);
  append_toffoli(wal) (i4,i5)(i0);
  append_toffoli(wal) (i0,i4,i5)(i1);
  append_toffoli(wal) (i4)(i0);
  append_toffoli(wal) (i0)(i4);
  append_toffoli(wal) (i5)(i1);
  append_toffoli(wal) (i1)(i5);

}

void revkit::wallace_mult()
{

  //-------------Hand Coded Full Adder---------------------------------
	std::cout<<"Total Input lines: "<<no_of_inputlines<<std::endl;
	circuit fadd(4*(bitsize+1));

	while(bitsize>=0)
	{
	  
	  std::vector<unsigned> ctrl;
	  ctrl.clear();
	  ctrl += pos(2),pos(3);
	  append_toffoli(fadd,ctrl,pos(0));
	  append_cnot(fadd,pos(2),pos(0));
	  ctrl.clear();
	  ctrl += pos(0),pos(1),pos(3);
	  append_toffoli(fadd,ctrl,pos(2));
	  append_cnot(fadd,pos(2),pos(0));
	  append_cnot(fadd,pos(3),pos(2));
	  append_cnot(fadd,pos(3),pos(1));
	  append_cnot(fadd,pos(3),pos(1));
	  ctrl.clear();
	  ctrl += pos(0),pos(1),pos(2);
	  append_toffoli(fadd,ctrl,pos(3));
	  append_cnot(fadd,pos(3),pos(1));
	  ctrl.clear();
	  ctrl += pos(1),pos(2);
	  append_toffoli(fadd, ctrl, pos(3));
	  append_cnot(fadd,pos(1),pos(0));
	  ctrl.clear();
	  ctrl +=pos(0),pos(2);
	  append_toffoli(fadd, ctrl, pos(1));
	  append_cnot(fadd,pos(1),pos(0));
	  append_cnot(fadd,pos(2),pos(1));
	  append_cnot(fadd,pos(2),pos(0));
	  append_cnot(fadd,pos(1),pos(3));
	  append_cnot(fadd,pos(1),pos(0));
	  append_cnot(fadd,pos(3),pos(1));
	  append_cnot(fadd,pos(0),pos(3));
	  append_cnot(fadd,pos(3),pos(0));
	  bitsize--;
	}

	  append_circuit(wallace,fadd);
	   
	  

	//  circuit_to_truth_table(fadd,faddspec, simple_simulation_func());
	//  write_specification(faddspec,"handcoded.spec");
	  //std::cout<< "Hand Coded Full Add\n"<< fadd;
	 
	 // simple_simulation(outp, wallace, inp);
	  
	  

	//----------Printing the output-----------
//	  print_output(inp,no_of_inputlines,"Input");
//	  print_output(outp,no_of_inputlines,"Intermediate Output");
	//---------Remapping----------------------



	 append_fredkin(wallace)(make_var(40))(29,1);
	 for(size_t i=5;i<32;i+=4)
	 {
		append_fredkin(wallace)(make_var(40))(i,i-4); 
	 }

	 for(size_t i=0;i<32;i+=4)
	 {
		append_fredkin(wallace)(make_var(40))(i,i+2); 
	 }

	 int k=32;
	 for(size_t i=3;i<32;i+=4)
	 {
		append_fredkin(wallace)(make_var(40))(i,k); 
		k++;
	 }
	 
	 k=41;
	 for(size_t i=0;i<32;i+=4)
	 {
		append_fredkin(wallace)(make_var(40))(i,k); 
		k++;
	 }
//	 simple_simulation(outp,wallace,inp);
//	 print_output(outp,no_of_inputlines,"Remapped Output After 1st CSA Level");

	//-----------Another Level of CSA----------
	 append_circuit(wallace,fadd);

	//---------Simulation----------------------
	 //simple_simulation(outp,wallace,inp);
	 //print_output(outp,no_of_inputlines,"Output After 2nd CSA Level");

	 
	//------------Carry Lookahead Addition-----------------------
	  int no_sumcarry_replicate = 18; 
	  int no_sumcarry_starting  = 49;
	  int sum_index = 0;
	  int carry_index = 5;
	  //Replicating the sum and carry bits to the bits 49->66
	  for(int k= no_sumcarry_starting; k < (no_sumcarry_replicate + no_sumcarry_starting - 3); k++) 
	  {
		if(k%2 != 0)
		{//Sum  
			append_cnot( wallace, sum_index, k );
			sum_index += 4;
		}
		else
		{//Carry
			append_cnot( wallace, carry_index, k );
			carry_index +=4;
		}
	  }
//	 simple_simulation(outp,wallace,inp);
//	 print_output(outp,no_of_inputlines,"Input to CLA\n");
	 
	 int line[18]={49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66};
	 int freecounter = 67;
	// add_pkillgate(wallace, line[14],line[15],line[16], line[17], freecounter++, freecounter++);
	 for(int i=17; i > 2; i = i - 4)
	 {
		add_pkillgate(wallace, line[i-3],line[i-2],line[i-1], line[i], freecounter++, freecounter++);
	 }

	 
//	 simple_simulation(outp,wallace,inp);

//	 print_output(outp,no_of_inputlines,"Output After 1st CLA Level");
	 
	 for(int i=15; i>2; i = i - 8)
	 {
		add_pkillgate(wallace, line[i-5], line[i-4], line[i-1], line[i], freecounter++, freecounter++);
	 }
	 
//	 simple_simulation(outp,wallace,inp);

//	 print_output(outp,no_of_inputlines,"Output After 2nd CLA Level");

	 add_pkillgate(wallace, line[2],line[3],line[10],line[11], freecounter++, freecounter++);

 
	 add_pkillgate(wallace, line[6],line[7],line[10],line[11], freecounter++, freecounter++);
	 
//	 simple_simulation(outp,wallace,inp);
//	 print_output(outp,no_of_inputlines,"Output After 3rd  CLA Level");

	 for(int i=15; i>0; i = i - 4)
	 {
		
		add_pkillgate(wallace, line[i-3],line[i-2],line[i-1],line[i], freecounter++, freecounter++);
	 }
	 
//	 simple_simulation(outp,wallace,inp);
//	 print_output(outp,no_of_inputlines,"Output After 4th CLA Level");

	 for(int i=0; i<7; i++)
	 {
		add_xor(wallace,4*i,(4*i)+5,52+(2*i));
	 }
	 add_xor(wallace,28,66,66); // Reusing the zero at Kill input (66th bit) of Prop-Kill Logic instead of a zero.
	 
//	 simple_simulation(outp,wallace,inp);
//	 print_output(outp,no_of_inputlines,"Final Output of CLA, and therefore the Wallace Tree");

	 //std::cout<< "Hand Coded Wallace\n"<< wallace;
}
/*
int main( int argc, char ** argv )
{
#if EXAMPLE_MODE > 0
  circuit circ;
  read_realization( circ, "circuit.real" );
  std::cout<< "Original Circuit" <<std::endl<< circ <<std::endl;
  reverse_circuit( circ );
  write_realization( circ, "circuit-copy.real" );
  circuit circrev,circrevcopy;
  std::vector<unsigned> filter {0,0,0};
  read_realization(circrev, "circuit-copy.real");
  std::cout <<"Reversed Circuit" << std::endl << circrev << std::endl;
  copy_circuit( circrev, circrevcopy);
  clear_circuit( circrev );
  std::cout <<"Cleared Circuit" << std::endl << circrev << std::endl;
  std::cout <<"Copied Circuit" << std::endl << circrevcopy << std::endl;
  
  for ( const gate& g : circrevcopy)
  {
    std::cout << "Gate Toffoli: "<< is_toffoli(g)<<" has " << g.controls().size() << " controls." << std::endl;

  }
  
  binary_truth_table specfile,newspec,fulladder,fulladdermod;
  circuit truthcirc;
  read_specification(specfile, "newfile.spec");
  transformation_based_synthesis(truthcirc, specfile);
  std::cout<< "Transformation: "<< std::endl<< truthcirc ;
  
  circuit_to_truth_table( truthcirc, newspec, simple_simulation_func() );
  write_specification(newspec, "transspec.spec");

  clear_circuit(truthcirc);
  reed_muller_synthesis(truthcirc, specfile);
  std::cout<< "Reed Muller: "<< std::endl<< truthcirc ;
  
  circuit_to_truth_table( truthcirc, newspec, simple_simulation_func() );
  write_specification(newspec, "reedspec.spec");


  clear_circuit(truthcirc);
  quantified_exact_synthesis(truthcirc, specfile);
  std::cout<< "Exact: "<< std::endl<< truthcirc ;
  
  circuit_to_truth_table( truthcirc, newspec, simple_simulation_func() );
  write_specification(newspec, "exactspec.spec");

  clear_circuit(truthcirc);
  read_specification(fulladder,"fulladder.spec");
  reed_muller_synthesis(truthcirc,fulladder);
  boost::dynamic_bitset<> outputs;
  boost::dynamic_bitset<> inputs(4,1ul);
  simple_simulation(outputs, truthcirc, inputs);
  std::cout<<"Add circuit\n"<<truthcirc;
  std::cout<<"Inputs: "<<inputs;
  std::cout<<"\nOutputs: "<<outputs<<std::endl;
   for(size_t i = 0; i < 64; i++)
   {
       if(inputs[i])
           std::cout << "Position " << i << " is 1" << std::endl;
    }

  for (std::vector<boost::dynamic_bitset<>>::const_iterator i = inputs.begin(); i != inputs.end(); ++i)
    std::cout << *i << ' ';
  std::cout<<"\nOutputs: ";
  for (std::vector<boost::dynamic_bitset<>>::const_iterator j = outputs.begin(); j != outputs.end(); ++j)
    std::cout << *j << ' ';
  std::cout<<"\n";
 
 //extend_truth_table(fulladder);
  //write_specification(fulladdermod,"modfulladder.spec");

#else

  std::cout<<"\nUSER PROGRAM EXECUTION\n";
  circuit circ[2];
  binary_truth_table circ1spec,faddspec;
  read_specification(circ1spec,"fulladder.spec");

  circuit circu( 5 );

  append_cnot( circu, 2, 3 );
  prepend_cnot( circu, 0, 1 );
  append_fredkin( circu )( make_var( 0 ), make_var( 1 ) )( 2, 4 );
  insert_cnot( circu, 2, 1, 2 );
  prepend_not( circu, 2 );

  std::vector<unsigned> ctrl;
  ctrl += 0,1,2,3;
  append_toffoli( circu, ctrl, 4 );
  std::cout<<"Circu\n"<<circu;
  
  reed_muller_synthesis(circ[0],circ1spec);
  std::cout<<"Circ0\n"<<circ[0];
  
  wallace_multiplier();

  
#endif
   
  return 0;

}
*/
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
