/* RevKit (www.revkit.org)
 * Copyright (C) 2009-2014  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */
#define EXAMPLE_MODE 1
#include <iostream>

#include <reversible/circuit.hpp>
#include <reversible/gate.hpp>
#include <reversible/target_tags.hpp>
#include <reversible/functions/reverse_circuit.hpp>
#include <reversible/functions/clear_circuit.hpp>
#include <reversible/functions/copy_circuit.hpp>
#include <reversible/io/read_realization.hpp>
#include <reversible/io/read_specification.hpp>
#include <reversible/io/write_specification.hpp>
#include <reversible/io/write_realization.hpp>
#include <reversible/io/print_circuit.hpp>
#include <reversible/synthesis/transformation_based_synthesis.hpp>
#include <reversible/synthesis/reed_muller_synthesis.hpp>
#include <reversible/synthesis/quantified_exact_synthesis.hpp>
#include <reversible/functions/circuit_to_truth_table.hpp>
#include <reversible/functions/extend_truth_table.hpp>
#include <reversible/simulation/simple_simulation.hpp>
#include <reversible/functions/add_circuit.hpp>
#include <reversible/functions/add_gates.hpp>
using namespace revkit;

int main( int argc, char ** argv )
{
#if EXAMPLE_MODE > 0
  circuit circ;
  read_realization( circ, "circuit.real" );
  std::cout<< "Original Circuit" <<std::endl<< circ <<std::endl;
  reverse_circuit( circ );
  write_realization( circ, "circuit-copy.real" );
  circuit circrev,circrevcopy;
  std::vector<unsigned> filter {0,0,0};
  read_realization(circrev, "circuit-copy.real");
  std::cout <<"Reversed Circuit" << std::endl << circrev << std::endl;
  copy_circuit( circrev, circrevcopy);
  clear_circuit( circrev );
  std::cout <<"Cleared Circuit" << std::endl << circrev << std::endl;
  std::cout <<"Copied Circuit" << std::endl << circrevcopy << std::endl;
 /* 
  for ( const gate& g : circrevcopy)
  {
    std::cout << "Gate Toffoli: "<< is_toffoli(g)<<" has " << g.controls().size() << " controls." << std::endl;

  }
  */
  binary_truth_table specfile,newspec,fulladder,fulladdermod;
  circuit truthcirc;
  read_specification(specfile, "xor.spec");
  transformation_based_synthesis(truthcirc, specfile);
//  write_specification(specfile,"output.spec");
  std::cout<< "Transformation: "<< std::endl<< truthcirc ;

  circuit propkill(6);
  std::vector<unsigned> c;
//  c+=1,2,3;
  int i0=0,i1=1,i2=2,i3=3,i4=4,i5=5;
  append_toffoli(propkill) (i1,i2,i3)(i4);
  append_toffoli(propkill) (i1,i2,i3,i5)(i4);
  append_toffoli(propkill) (i0,i1,i2,i3)(i4);
  append_toffoli(propkill) (i1,i2,i3,i4)(i0);
  append_toffoli(propkill) (i0,i2,i3,i4,i5)(i1);
  append_toffoli(propkill) (i0,i1,i2,i3,i5)(i4);
  append_toffoli(propkill) (i0,i1)(i5);
  
  append_toffoli(propkill) (i0,i2,i3,i5)(i4);
  append_toffoli(propkill) (i0,i2,i5)(i1);
  append_toffoli(propkill) (i0,i3,i5)(i1);
  append_toffoli(propkill) (i0,i5)(i1);
  append_toffoli(propkill) (i0,i1)(i5);
  append_toffoli(propkill) (i0)(i4);
  
  append_toffoli(propkill) (i1,i2,i4,i5)(i0);
  append_toffoli(propkill) (i1,i3,i4,i5)(i0);
  append_toffoli(propkill) (i1,i4,i5)(i0);
  
  
  append_toffoli(propkill) (i1)(i5);
  append_toffoli(propkill) (i4,i2,i3)(i0);
  append_toffoli(propkill) (i0,i2,i3,i4)(i1);
  append_toffoli(propkill) (i5,i2,i3)(i4);
  append_toffoli(propkill) (i4,i2,i5)(i0);
  append_toffoli(propkill) (i0,i2,i4,i5)(i1);
  append_toffoli(propkill) (i2,i4)(i0);
  append_toffoli(propkill) (i5,i2)(i1);
  
  
  append_toffoli(propkill) (i3,i4,i5)(i0);
  append_toffoli(propkill) (i0,i3,i5,i4)(i1);
  append_toffoli(propkill) (i3,i4)(i0);
  append_toffoli(propkill) (i3,i5)(i1);
  append_toffoli(propkill) (i4,i5)(i0);
  append_toffoli(propkill) (i0,i4,i5)(i1);
  append_toffoli(propkill) (i4)(i0);
  append_toffoli(propkill) (i0)(i4);
  append_toffoli(propkill) (i5)(i1);
  
  
  append_toffoli(propkill) (i1)(i5);
  
  std::cout<<std::endl<<propkill<<std::endl;
  

  circuit_to_truth_table( propkill, newspec, simple_simulation_func() );
  write_specification(specfile,"output.spec");
  /*
  circuit_to_truth_table( truthcirc, newspec, simple_simulation_func() );
  write_specification(newspec, "transspec.spec");

  clear_circuit(truthcirc);
  reed_muller_synthesis(truthcirc, specfile);
  std::cout<< "Reed Muller: "<< std::endl<< truthcirc ;
  
  circuit_to_truth_table( truthcirc, newspec, simple_simulation_func() );
  write_specification(newspec, "reedspec.spec");


  clear_circuit(truthcirc);
  quantified_exact_synthesis(truthcirc, specfile);
  std::cout<< "Exact: "<< std::endl<< truthcirc ;
  
  circuit_to_truth_table( truthcirc, newspec, simple_simulation_func() );
  write_specification(newspec, "exactspec.spec");

  read_specification(fulladder,"fulladder.spec");
  //extend_truth_table(fulladder);
  //write_specification(fulladdermod,"modfulladder.spec");
*/
#else

  std::cout<<"\nUSER PROGRAM EXECUTION\n";
  circuit circ[2];
  std::vector<unsigned> controls;
  controls+=0,1;
  binary_truth_table circ1spec;
  read_specification(circ1spec,"fulladder.spec");

  circuit circu( 5 );

 // append_cnot( circu, 2, 3 );
 // prepend_cnot( circu, 0, 1 );
   append_fredkin( circu )(0,make_var(1) )( 2, 4 );
 // insert_cnot( circu, 2, 1, 2 );
 // prepend_not( circu, 2 );

 // std::vector<unsigned> ctrl;
 // controls += 0,1,2,3;
 // append_toffoli( circu, ctrl, 4 );
  
  //std::cout<<"Circu\n"<<circu;
  //reed_muller_synthesis(circ[0],circ1spec);
  //reed_muller_synthesis(circ[1],circ1spec);
  //append_circuit(circu,circ[1]);
  //prepend_circuit(circu,circ[0]);
  //insert_circuit(circu,2,circ[0]);
  //std::cout<<"Circ0\n"<<circ[0];
  boost::dynamic_bitset<> out;
  boost::dynamic_bitset<> in(5,4ul);
  simple_simulation(out,circu,in);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<5;i++)
  {
	std::cout<<in[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<5;i++)
  {
	std::cout<<out[i]<<" ";
  }
  
  
  std::cout<<"\ncircu\n"<<circu;
#endif

  
  return 0;

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
