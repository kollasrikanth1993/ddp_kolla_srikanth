/* RevKit (www.revkit.org)
 * Copyright (C) 2009-2014  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#include <iostream>

#include <reversible/circuit.hpp>
#include <reversible/or.hpp>
#include <reversible/and.hpp>
#include <reversible/functions/add_gates.hpp>
#include <reversible/io/print_circuit.hpp>
#include <reversible/simulation/simple_simulation.hpp>
#include <reversible/io/read_specification.hpp>
#include <reversible/synthesis/transformation_based_synthesis.hpp>

using namespace revkit;

int main( int argc, char ** argv )
{
  circuit circ1( 4 );
  append_AND( circ1, 0, 1, 2, 3 );
  std::cout << circ1 << std::endl;
  
  boost::dynamic_bitset<> out;
  boost::dynamic_bitset<> in(4);
  in[3] = 1;
  simple_simulation(out,circ1,in);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<in[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<out[i]<<" ";
  }

  boost::dynamic_bitset<> in1(4);
  in1[2] = 1;
  in1[3] = 1;
  simple_simulation(out,circ1,in1);
  std::cout<<"\nInput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<in1[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<out[i]<<" ";
  }

  boost::dynamic_bitset<> in2(4);
  in2[1] = 1;
  in2[3] = 1;
  simple_simulation(out,circ1,in2);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<in2[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<out[i]<<" ";
  }

  boost::dynamic_bitset<> in3(4);
  in3[1] = 1;
  in3[2] = 1;
  in3[3] = 1;
  simple_simulation(out,circ1,in3);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<in3[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<out[i]<<" ";
  }

  boost::dynamic_bitset<> in4(4);
  in4[0] = 1;
  in4[3] = 1;
  simple_simulation(out,circ1,in4);
  std::cout<<"\nInput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<in4[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<out[i]<<" ";
  }

  boost::dynamic_bitset<> in5(4);
  in5[0] = 1;
  in5[2] = 1;
  in5[3] = 1;
  simple_simulation(out,circ1,in5);
  
  std::cout<<"\nInput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<in5[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<out[i]<<" ";
  }

  boost::dynamic_bitset<> in6(4);
  in6[0] = 1;
  in6[1] = 1;
  in6[3] = 1;
  simple_simulation(out,circ1,in6);
  std::cout<<"\nInput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<in6[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<out[i]<<" ";
  }

  boost::dynamic_bitset<> in7(4);
  in7[0] = 1;
  in7[1] = 1;
  in7[2] = 1;
  in7[3] = 1;
  simple_simulation(out,circ1,in7);
  std::cout<<"\nInput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<in7[i]<<" ";
  }
  
  std::cout<<"\noutput: ";
  for(size_t i=0;i<4;i++)
  {
	std::cout<<out[i]<<" ";
  }
  return 0;

}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
