/* RevKit (www.revkit.org)
 * Copyright (C) 2009-2014  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#include <iostream>

#include <reversible/xor.hpp>
#include <reversible/and.hpp>
#include <reversible/or.hpp>
#include <reversible/simulation/simple_simulation.hpp>

using namespace revkit;

int main( int argc, char ** argv )
{
	int zero_line = 0;
	int one_line = 1;
	int no_inputlines = 192, init_start = 2;
	int b_start = init_start, d_start = b_start + 32, not_1_drive_start = d_start + 32, c_start = not_1_drive_start + 32;
	int bdaux_start = c_start + 32, cbdaux_start = bdaux_start + 32;
	circuit circ1(no_inputlines);

	
	// Circuit for Negating D (~D) -- output into not_1_drive lines
	for(int i = 0; i < 32; i++){
		append_toffoli( circ1 )( d_start + i )( not_1_drive_start + i );
	}

	// For calculating (B OR ~D)
	for(int i = 0; i < 32; i++){
		append_OR( circ1, bdaux_start + i, b_start + i, not_1_drive_start + i);	// All aux inputs have to be driven to 0, enable is 0
	}
	
	// Circuit for C XOR (B OR ~D)
	for(int i = 0; i < 32; i++){
		append_AND( circ1, cbdaux_start + i, not_1_drive_start + i, c_start + i, one_line);
	}

	// Output lines will be c_start + i

	std::cout << circ1 << std::endl;
	
	


  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
