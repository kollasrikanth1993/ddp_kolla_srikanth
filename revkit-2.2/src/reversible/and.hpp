#include <iostream>

#include <reversible/circuit.hpp>
#include <reversible/functions/add_gates.hpp>
#include <reversible/io/print_circuit.hpp>
#include <reversible/io/read_specification.hpp>
#include <reversible/synthesis/transformation_based_synthesis.hpp>


namespace revkit{

	circuit& append_AND(circuit& circ, int i0, int i1, int i2, int one_line){
		/*binary_truth_table specfile;
  		read_specification(specfile, "And.spec");
  		transformation_based_synthesis(circ, specfile);*/
		append_toffoli( circ )( i2 )( i1 );
		append_toffoli( circ )( i1 )( i2 );
		append_toffoli( circ )( i0 )( i2 );
		append_toffoli( circ )( i0, i2 )( i1 );
		append_toffoli( circ )( i1, i2 )( i0 );
		append_toffoli( circ )( i0, i2 )( i1 );
		append_toffoli( circ )( i0 )( i2 );
		append_toffoli( circ )( one_line )( i1);
		//append_toffoli( circ )( )( 1 );
		//std::cout << circ << std::endl;
		return circ;
	}
}


