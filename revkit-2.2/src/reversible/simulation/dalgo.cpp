/* RevKit (www.revkit.org)
 * Copyright (C) 2009-2014  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dalgo.hpp"

#include <core/utils/timer.hpp>

#include <reversible/gate.hpp>
#include <reversible/target_tags.hpp>

#define DEBUG 0
namespace revkit
{

	int globalmax;
void print_input(int* input, const int size)
{
	for(int i=0;i<size;i++)
	{
		std::cout<<" "<<input[i];
	}
	std::cout<<std::endl;
}

void dalgo_core_gate_simulation( gate& g, int* input,int* output, const int size )
{	
	if ( is_toffoli( g ) )
	{
	#if DEBUG
	std::cout<<"\nEncountered a Toffoli Gate with target: "<<g.targets().front()<<" and controls ";
	#endif
	//	print_input(input,size);
	int c_mask[size];
	for(int i=0;i<size;i++)
	{
		c_mask[i] = 0;
	}
	int input_copy[size];
	for(int i=0;i<size;i++)
	{	
		input_copy[i] = input[i];
		//output[i]     = input[i];
	}
	for ( const auto& v : g.controls() )
	{
		if ( !v.polarity() )
		{
		  input_copy[v.line()] = input_copy[v.line()]?0:1;
		}
		#if DEBUG
		std::cout<<v.line()<<" ";
		#endif
		c_mask[v.line()] = 1;
		output[v.line()] = input[v.line()];	
	}
	#if DEBUG
	std::cout<<std::endl;
	#endif
	bool  cmasknotset = true;
	bool maskmatch = true;
	for(int i=0;i<size;i++)
	{
		if(c_mask[i]==1)
		{
			cmasknotset = false;
		}
		if((input_copy[i] && c_mask[i])!=c_mask[i])
		{
			maskmatch = false;
		}
	}
	if ( cmasknotset || maskmatch )
	{
	   output[g.targets().front()] = input[g.targets().front()]?0:1;
	}
	else
	{
	output[g.targets().front()] = input[g.targets().front()];
	}
	//      std::cout<<" Output: ";
	//    print_input(output,size);
	}
	// TODO negative controls
	else if ( is_fredkin( g ) )
	{

	int c_mask[size];
	for(int i=0;i<size;i++)
	{
	   c_mask[i] = 0;
	}

	for ( const auto& v : g.controls() )
	{
	assert( v.polarity() );
	c_mask[v.line()] = 1;
	output[v.line()] = input[v.line()];
	}
	bool  cmasknotset = true;
	bool maskmatch = true;
	for(int i=0;i<size;i++)
	{
		if(c_mask[i]==1)
		{
			cmasknotset = false;
		}
		if((input[i] && c_mask[i])!=c_mask[i])
		{
			maskmatch = false;
		}
	}
	// get both positions and values
	unsigned t1 = g.targets().at( 0u );
	unsigned t2 = g.targets().at( 1u );
	if ( cmasknotset || maskmatch )
	{

	bool t1v = input[ t1];
	bool t2v = input[ t2];

	// only swap when different
if ( t1v != t2v )
	{
	  output[ t1] = t2v ;
	  output[ t2] = t1v ;
	}
	}
	else
	{
	output[t1] = input[t1];
	output[t2] = input[t2];
	}

	#if DEBUG
	std::cout<<"\nEncountered a Fredkin Gate\n";
	#endif
	}
	else if ( is_peres( g ) )
	{
	/*      if ( input.test( g.controls().front(i).line() ) ) // is single control set
	      {
		// get both positions and value of t1
		unsigned t1 = g.targets().at( 0u );
		unsigned t2 = g.targets().at( 1u );

		bool t1v = input.test( t1 );

		 flip t1 
		input.flip( t1 );

		 flip t2 if t1 was true 
		if ( t1v )
		{
		  input.flip( t2 );
		}
	      }
	*/
	std::cout<<"\nEncountered a Peres Gate\n";
	}
	else if ( is_module( g ) )
	{
	std::cout<<"\nEncountered a Module Gate\n";
	// pkarthik: VERIFY AND INCLUDE THIS LOGIC
	/*   int* c_mask( input.size() );
	for ( const auto& v : g.controls() )
	{
	assert( v.polarity() );
	c_mask.set( v.line() );
	}

	// cancel if controls are not hit
	if ( !c_mask.is_subset_of( input ) )
	{
	return input;
	}

	const module_tag* tag = boost::any_cast<module_tag>( &g.type() );

	// TODO write test case
	// get the new input sub pattern
	int* tpattern( g.targets().size() );
	for ( const auto& i : g.targets() )
	{
	tpattern.set( i, input.test( i ) );
	}
	int* toutput;
	//assert( dalgo_simulation( toutput, *tag->reference, tpattern ) );

	for ( const auto& i : g.targets() )
	{
	input.set( i, toutput.test( i ) );
	}

	return input;
	*/
	}
	else
	{
		assert( false );
	}

}
int level_order(circuit::iterator first, circuit::iterator last, const int size)
{
	int wires[size], maxlevel;
	globalmax = 0;
	for(int i=0;i<size;i++)
	{
		wires[i]=0;
	}
	int count = 0;
	while(first != last)
	{
	maxlevel=0;
		//std::cout<<"\ncontrols ";
	for ( const auto& v : (*first).controls() )
	{
		maxlevel = (wires[v.line()]>maxlevel)?wires[v.line()]:maxlevel;
		wires[v.line()]++;
		//std::cout<<v.line()<<" ";
	}
	//std::cout<<"targets ";
	for ( const auto& v : (*first).targets() )
	{
		maxlevel = (wires[v]>maxlevel)?wires[v]:maxlevel;
		wires[v]++;
		//std::cout<<v<<" ";
	}
	for ( const auto& v : (*first).controls() )
	{
		wires[v.line()] = maxlevel + 1;
	//	std::cout<<v.line()<<" ";
	}
	//std::cout<<"targets ";
	for ( const auto& v : (*first).targets() )
	{
		wires[v] = maxlevel + 1;
		//	std::cout<<v<<" ";
		}
		(*first).set_level(maxlevel);
		//std::cout<<"\nGate Level: "<<(*first).getlevel()<<std::endl;
		if(maxlevel>globalmax) globalmax = maxlevel;
		first++;
		count++;
	}
	#if DEBUG
	std::cout<<"Level Order Processed Gate Count: "<<count<<std::endl;
	#endif
	return globalmax;	
}
bool dalgo_simulation( int* output, Gatelist* circuit_new, Level* snapshot, int* input, const int size) /*,
		  properties::ptr settings,
		  properties::ptr statistics )*/
{

	Gatelist* temp;
	//std::cout<<"\n=====================Simulation Starts===========================\n";	
	for(int i=0;i<(globalmax+1); i++)
	{
		for(int m=0; m<size; m++)
			snapshot[i+1].value[m] = -1;
		int j=0;
		temp = &circuit_new[i];
		while(temp != NULL)
		{
		//	std::cout<<"\nNode no: "<<j<<"\n";
			dalgo_core_gate_simulation(*(temp->g), snapshot[i].value, snapshot[i+1].value, size);
			temp = temp->next; 
			j++;
		}
		for(int m=0;m<size;m++)
		{
			if(snapshot[i+1].value[m] == -1) 
			{
				snapshot[i+1].value[m] = snapshot[i].value[m];
			}
		}	//	#if DEBUG
		//std::cout<<"Level Output: ";
		//print_input(snapshot[i+1].value, size);
		//std::cout<<std::endl;			
		//#endif
	}
	return true;
}
void insert_into_gatelist(Gatelist& gl, gate& g)
{
	Gatelist *temp, *ptr;
	temp = &gl;
	#if DEBUG	
	std::cout<<"\nNew node has target as: "<<g.targets().at(0u);
	#endif
	if(temp->g == NULL)
	{
		gl.g = &g;
		gl.next = NULL;
	}	
	else
	{
		while(temp->next != NULL)
		{	
			temp = temp->next;
		}
		ptr = new Gatelist;
		ptr->g = &g;
		ptr->next = NULL;
		temp->next = ptr;
	}
}
void create_new_datastruct(Gatelist* circuit_new, circuit circ)
{
	circuit::iterator first = circ.begin();
	circuit::iterator last = circ.end();
	int count = 0;
	while(first != last)
	{	
		int currentlevel = (*first).getlevel();
#if DEBUG
		std::cout<<"\nmylevel: "<<currentlevel<<" my target: "<<(*first).targets().front();
#endif
		insert_into_gatelist(circuit_new[currentlevel],*first); 
		first++;
		count++;
	}
#if DEBUG 
	std::cout<<"Create New DS Processed Gate Count: "<<count<<std::endl;
#endif
}
/*=============================================================================
dalgo_simulation()
- Top Level simulation function that accepts a revkit::circuit, input string and
an output string.
- If the input is given, it simulates the circuit and produces the output
  in int* output. 
- Alternatively, a partially specified input as well as output can be given. It
  will perform an imply and check and infer the don't care values
- Logic Values supported: 0 - Zero, 1 - One, 2 - Don't care
- Algorithm: 
	1. level order the given revkit::circuit to annotate every gate with its
	corresponding level.
	2. 
===============================================================================*/
bool dalgo_simulation( int* output, circuit& circ, int* input, const int size)
{ 

	//std::cout<<"\n======================DALGO========================\n";
	/*==========Snapshot of logic values of the circuit( 2D Matrix)====*/
	Level* snapshot;
	bool flag= false;
	/*==========Level order the gates in revkit::circuit===============*/
	globalmax = level_order(circ.begin(), circ.end(),size);
	snapshot = new Level[globalmax + 2];
	for(int i = 0; i < globalmax + 2; i++){
		snapshot[i].value = new int[size];
	}
	/*==========New representation of the circuit: Gatelist============*/
	Gatelist* circuit_new;
	circuit_new = new Gatelist[globalmax + 1];
#if DEBUG
	std::cout<<"\nMax Levels in the circuit: "<<globalmax<<std::endl;
#endif
	/*==========Initializing the Gatelist structure====================*/
	for(int i=0;i< (globalmax + 1);i++)
	{
		circuit_new[i].next = NULL;
		circuit_new[i].g = NULL;
	}
	/*==========Populate the Gatelist structure from revkit::circuit===*/
	create_new_datastruct(circuit_new, circ);

	std::cout<<"\nInput:\n";
	/*==========Feed the input into first level of snapshot============*/
	for(int i=0;i<size;i++)
	{	
		snapshot[0].value[i] = input[i];
		std::cout<<input[i]<<" ";
	}
  
	std::cout<<std::endl;
	/*==========Perform simulation/imply and check====================*/
	flag =  dalgo_simulation( output, circuit_new ,snapshot, input,size);
	std::cout<<"Output:\n";
	/*==========Tap the output from the last level of snapshot========*/
	for(int i=0;i<size;i++)
	{	
		output[i] = snapshot[globalmax + 1].value[i];
		std::cout<<output[i]<<" ";
	}  
	std::cout<<"\n";
	return flag;
}

}//END of REVKIT NAMESPACE
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
