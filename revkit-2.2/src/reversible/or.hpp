#include <iostream>

#include <reversible/circuit.hpp>
#include <reversible/functions/add_gates.hpp>
#include <reversible/io/print_circuit.hpp>


namespace revkit{

	circuit& append_OR(circuit& circ, int i0, int i1, int i2){
		append_toffoli( circ )( i0 )( i1 );
		append_toffoli( circ )( i1 )( i0 );
		append_toffoli( circ )( i0, i1 )( i2 );
		append_toffoli( circ )( i0, i2 )( i1 );
		append_toffoli( circ )( i1, i2 )( i0 );
		append_toffoli( circ )( i0, i1 )( i2 );
		append_toffoli( circ )( i2 )( i0 );
		//std::cout << circ << std::endl;
		return circ;
	}
}


