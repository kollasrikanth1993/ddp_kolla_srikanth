# CMake generated Testfile for 
# Source directory: /home/srikanth/revkit-2.2/test
# Build directory: /home/srikanth/revkit-2.2/build/test
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(esop_minimization "test_esop_minimization")
ADD_TEST(esop_synthesis "test_esop_synthesis")
ADD_TEST(redundancy_functions "test_redundancy_functions")
ADD_TEST(synthesis "test_synthesis")
ADD_TEST(permutation "test_permutation")
ADD_TEST(circuit "test_circuit")
ADD_TEST(copy_circuit "test_copy_circuit")
ADD_TEST(restricted_growth_sequence "test_restricted_growth_sequence")
ADD_TEST(rcbdd_scalability "test_rcbdd_scalability")
ADD_TEST(truth_table_based_synthesis "test_truth_table_based_synthesis")
ADD_TEST(circuit_io "test_circuit_io")
ADD_TEST(truth_table "test_truth_table")
ADD_TEST(infrastructure "test_infrastructure")
ADD_TEST(cubes "test_cubes")
