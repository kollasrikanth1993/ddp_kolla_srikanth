# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/bin/cmake

# The command to remove a file.
RM = /usr/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/srikanth/revkit-2.2

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/srikanth/revkit-2.2/build

# Include any dependencies generated for this target.
include test/CMakeFiles/test_cubes.dir/depend.make

# Include the progress variables for this target.
include test/CMakeFiles/test_cubes.dir/progress.make

# Include the compile flags for this target's objects.
include test/CMakeFiles/test_cubes.dir/flags.make

test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o: test/CMakeFiles/test_cubes.dir/flags.make
test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o: ../test/core/cubes.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/srikanth/revkit-2.2/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o"
	cd /home/srikanth/revkit-2.2/build/test && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/test_cubes.dir/core/cubes.cpp.o -c /home/srikanth/revkit-2.2/test/core/cubes.cpp

test/CMakeFiles/test_cubes.dir/core/cubes.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/test_cubes.dir/core/cubes.cpp.i"
	cd /home/srikanth/revkit-2.2/build/test && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/srikanth/revkit-2.2/test/core/cubes.cpp > CMakeFiles/test_cubes.dir/core/cubes.cpp.i

test/CMakeFiles/test_cubes.dir/core/cubes.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/test_cubes.dir/core/cubes.cpp.s"
	cd /home/srikanth/revkit-2.2/build/test && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/srikanth/revkit-2.2/test/core/cubes.cpp -o CMakeFiles/test_cubes.dir/core/cubes.cpp.s

test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o.requires:
.PHONY : test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o.requires

test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o.provides: test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o.requires
	$(MAKE) -f test/CMakeFiles/test_cubes.dir/build.make test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o.provides.build
.PHONY : test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o.provides

test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o.provides.build: test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o

# Object files for target test_cubes
test_cubes_OBJECTS = \
"CMakeFiles/test_cubes.dir/core/cubes.cpp.o"

# External object files for target test_cubes
test_cubes_EXTERNAL_OBJECTS =

test/test_cubes: test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o
test/test_cubes: test/CMakeFiles/test_cubes.dir/build.make
test/test_cubes: /usr/local/lib/libboost_unit_test_framework.so
test/test_cubes: /usr/local/lib/libboost_regex.so
test/test_cubes: /usr/local/lib/libboost_filesystem.so
test/test_cubes: /usr/local/lib/libboost_graph.so
test/test_cubes: /usr/local/lib/libboost_program_options.so
test/test_cubes: /usr/local/lib/libboost_system.so
test/test_cubes: src/librevkit_classical.so
test/test_cubes: src/librevkit_reversible.so
test/test_cubes: src/librevkit_core.so
test/test_cubes: /usr/local/lib/libboost_unit_test_framework.so
test/test_cubes: /usr/local/lib/libboost_regex.so
test/test_cubes: /usr/local/lib/libboost_filesystem.so
test/test_cubes: /usr/local/lib/libboost_graph.so
test/test_cubes: /usr/local/lib/libboost_program_options.so
test/test_cubes: /usr/local/lib/libboost_system.so
test/test_cubes: test/CMakeFiles/test_cubes.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable test_cubes"
	cd /home/srikanth/revkit-2.2/build/test && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/test_cubes.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
test/CMakeFiles/test_cubes.dir/build: test/test_cubes
.PHONY : test/CMakeFiles/test_cubes.dir/build

test/CMakeFiles/test_cubes.dir/requires: test/CMakeFiles/test_cubes.dir/core/cubes.cpp.o.requires
.PHONY : test/CMakeFiles/test_cubes.dir/requires

test/CMakeFiles/test_cubes.dir/clean:
	cd /home/srikanth/revkit-2.2/build/test && $(CMAKE_COMMAND) -P CMakeFiles/test_cubes.dir/cmake_clean.cmake
.PHONY : test/CMakeFiles/test_cubes.dir/clean

test/CMakeFiles/test_cubes.dir/depend:
	cd /home/srikanth/revkit-2.2/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/srikanth/revkit-2.2 /home/srikanth/revkit-2.2/test /home/srikanth/revkit-2.2/build /home/srikanth/revkit-2.2/build/test /home/srikanth/revkit-2.2/build/test/CMakeFiles/test_cubes.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : test/CMakeFiles/test_cubes.dir/depend

