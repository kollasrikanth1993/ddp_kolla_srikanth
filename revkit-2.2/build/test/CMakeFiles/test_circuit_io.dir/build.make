# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/bin/cmake

# The command to remove a file.
RM = /usr/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/srikanth/revkit-2.2

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/srikanth/revkit-2.2/build

# Include any dependencies generated for this target.
include test/CMakeFiles/test_circuit_io.dir/depend.make

# Include the progress variables for this target.
include test/CMakeFiles/test_circuit_io.dir/progress.make

# Include the compile flags for this target's objects.
include test/CMakeFiles/test_circuit_io.dir/flags.make

test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o: test/CMakeFiles/test_circuit_io.dir/flags.make
test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o: ../test/reversible/circuit_io.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/srikanth/revkit-2.2/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o"
	cd /home/srikanth/revkit-2.2/build/test && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o -c /home/srikanth/revkit-2.2/test/reversible/circuit_io.cpp

test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.i"
	cd /home/srikanth/revkit-2.2/build/test && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/srikanth/revkit-2.2/test/reversible/circuit_io.cpp > CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.i

test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.s"
	cd /home/srikanth/revkit-2.2/build/test && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/srikanth/revkit-2.2/test/reversible/circuit_io.cpp -o CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.s

test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o.requires:
.PHONY : test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o.requires

test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o.provides: test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o.requires
	$(MAKE) -f test/CMakeFiles/test_circuit_io.dir/build.make test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o.provides.build
.PHONY : test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o.provides

test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o.provides.build: test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o

# Object files for target test_circuit_io
test_circuit_io_OBJECTS = \
"CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o"

# External object files for target test_circuit_io
test_circuit_io_EXTERNAL_OBJECTS =

test/test_circuit_io: test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o
test/test_circuit_io: test/CMakeFiles/test_circuit_io.dir/build.make
test/test_circuit_io: /usr/local/lib/libboost_unit_test_framework.so
test/test_circuit_io: /usr/local/lib/libboost_regex.so
test/test_circuit_io: /usr/local/lib/libboost_filesystem.so
test/test_circuit_io: /usr/local/lib/libboost_graph.so
test/test_circuit_io: /usr/local/lib/libboost_program_options.so
test/test_circuit_io: /usr/local/lib/libboost_system.so
test/test_circuit_io: src/librevkit_classical.so
test/test_circuit_io: src/librevkit_reversible.so
test/test_circuit_io: src/librevkit_core.so
test/test_circuit_io: /usr/local/lib/libboost_unit_test_framework.so
test/test_circuit_io: /usr/local/lib/libboost_regex.so
test/test_circuit_io: /usr/local/lib/libboost_filesystem.so
test/test_circuit_io: /usr/local/lib/libboost_graph.so
test/test_circuit_io: /usr/local/lib/libboost_program_options.so
test/test_circuit_io: /usr/local/lib/libboost_system.so
test/test_circuit_io: test/CMakeFiles/test_circuit_io.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable test_circuit_io"
	cd /home/srikanth/revkit-2.2/build/test && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/test_circuit_io.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
test/CMakeFiles/test_circuit_io.dir/build: test/test_circuit_io
.PHONY : test/CMakeFiles/test_circuit_io.dir/build

test/CMakeFiles/test_circuit_io.dir/requires: test/CMakeFiles/test_circuit_io.dir/reversible/circuit_io.cpp.o.requires
.PHONY : test/CMakeFiles/test_circuit_io.dir/requires

test/CMakeFiles/test_circuit_io.dir/clean:
	cd /home/srikanth/revkit-2.2/build/test && $(CMAKE_COMMAND) -P CMakeFiles/test_circuit_io.dir/cmake_clean.cmake
.PHONY : test/CMakeFiles/test_circuit_io.dir/clean

test/CMakeFiles/test_circuit_io.dir/depend:
	cd /home/srikanth/revkit-2.2/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/srikanth/revkit-2.2 /home/srikanth/revkit-2.2/test /home/srikanth/revkit-2.2/build /home/srikanth/revkit-2.2/build/test /home/srikanth/revkit-2.2/build/test/CMakeFiles/test_circuit_io.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : test/CMakeFiles/test_circuit_io.dir/depend

