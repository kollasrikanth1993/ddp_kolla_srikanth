

set(ENV{VS_UNICODE_OUTPUT} "")
set(command "make;puma;-C;bin")
execute_process(
  COMMAND ${command}
  RESULT_VARIABLE result
  OUTPUT_FILE "/home/srikanth/revkit-2.2/build/ext/puma-prefix/src/puma-stamp/puma-build-out.log"
  ERROR_FILE "/home/srikanth/revkit-2.2/build/ext/puma-prefix/src/puma-stamp/puma-build-err.log"
  )
if(result)
  set(msg "Command failed: ${result}\n")
  foreach(arg IN LISTS command)
    set(msg "${msg} '${arg}'")
  endforeach(arg)
  set(msg "${msg}\nSee also\n  /home/srikanth/revkit-2.2/build/ext/puma-prefix/src/puma-stamp/puma-build-*.log\n")
  message(FATAL_ERROR "${msg}")
else()
  set(msg "puma build command succeeded.  See also /home/srikanth/revkit-2.2/build/ext/puma-prefix/src/puma-stamp/puma-build-*.log\n")
  message(STATUS "${msg}")
endif()
