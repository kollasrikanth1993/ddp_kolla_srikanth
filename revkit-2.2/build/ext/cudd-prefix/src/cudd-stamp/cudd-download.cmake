

set(ENV{VS_UNICODE_OUTPUT} "")
set(command "/usr/local/bin/cmake;-Dmake=${make};-Dconfig=${config};-P;/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-stamp/cudd-download-impl.cmake")
execute_process(
  COMMAND ${command}
  RESULT_VARIABLE result
  OUTPUT_FILE "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-stamp/cudd-download-out.log"
  ERROR_FILE "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-stamp/cudd-download-err.log"
  )
if(result)
  set(msg "Command failed: ${result}\n")
  foreach(arg IN LISTS command)
    set(msg "${msg} '${arg}'")
  endforeach(arg)
  set(msg "${msg}\nSee also\n  /home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-stamp/cudd-download-*.log\n")
  message(FATAL_ERROR "${msg}")
else()
  set(msg "cudd download command succeeded.  See also /home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-stamp/cudd-download-*.log\n")
  message(STATUS "${msg}")
endif()
