# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/epd/epd.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_epd.dir/epd/epd.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/cudd"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/epd"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/dddmp"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/mtr"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/obj"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/st"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/mnemosyne"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
