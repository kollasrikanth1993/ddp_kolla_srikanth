# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/cpu_stats.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/cpu_stats.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/cpu_time.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/cpu_time.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/datalimit.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/datalimit.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/pathsearch.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/pathsearch.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/pipefork.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/pipefork.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/prtime.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/prtime.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/ptime.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/ptime.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/restart.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/restart.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/safe_mem.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/safe_mem.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/saveimage.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/saveimage.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/state.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/state.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/strsav.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/strsav.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/stub.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/stub.c.o"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/texpand.c" "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/CMakeFiles/cudd_util.dir/util/texpand.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/cudd"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/epd"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/dddmp"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/mtr"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/obj"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/st"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util"
  "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/mnemosyne"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
