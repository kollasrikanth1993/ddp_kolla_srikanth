# Install script for directory: /home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/srikanth/revkit-2.2/ext")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/srikanth/revkit-2.2/ext/include/cudd.h;/home/srikanth/revkit-2.2/ext/include/cuddInt.h;/home/srikanth/revkit-2.2/ext/include/cuddObj.hh;/home/srikanth/revkit-2.2/ext/include/dddmp.h;/home/srikanth/revkit-2.2/ext/include/epd.h;/home/srikanth/revkit-2.2/ext/include/mtr.h;/home/srikanth/revkit-2.2/ext/include/st.h;/home/srikanth/revkit-2.2/ext/include/util.h")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/srikanth/revkit-2.2/ext/include" TYPE FILE FILES
    "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/cudd/cudd.h"
    "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/cudd/cuddInt.h"
    "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/obj/cuddObj.hh"
    "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/dddmp/dddmp.h"
    "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/epd/epd.h"
    "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/mtr/mtr.h"
    "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/st/st.h"
    "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd/util/util.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_cudd.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_cudd.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_cudd.so"
         RPATH "")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/srikanth/revkit-2.2/ext/lib/libcudd_cudd.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/srikanth/revkit-2.2/ext/lib" TYPE SHARED_LIBRARY FILES "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/libcudd_cudd.so")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_cudd.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_cudd.so")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_cudd.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_dddmp.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_dddmp.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_dddmp.so"
         RPATH "")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/srikanth/revkit-2.2/ext/lib/libcudd_dddmp.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/srikanth/revkit-2.2/ext/lib" TYPE SHARED_LIBRARY FILES "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/libcudd_dddmp.so")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_dddmp.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_dddmp.so")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_dddmp.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_mtr.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_mtr.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_mtr.so"
         RPATH "")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/srikanth/revkit-2.2/ext/lib/libcudd_mtr.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/srikanth/revkit-2.2/ext/lib" TYPE SHARED_LIBRARY FILES "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/libcudd_mtr.so")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_mtr.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_mtr.so")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_mtr.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_st.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_st.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_st.so"
         RPATH "")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/srikanth/revkit-2.2/ext/lib/libcudd_st.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/srikanth/revkit-2.2/ext/lib" TYPE SHARED_LIBRARY FILES "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/libcudd_st.so")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_st.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_st.so")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_st.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_util.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_util.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_util.so"
         RPATH "")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/srikanth/revkit-2.2/ext/lib/libcudd_util.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/srikanth/revkit-2.2/ext/lib" TYPE SHARED_LIBRARY FILES "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/libcudd_util.so")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_util.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_util.so")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_util.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_epd.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_epd.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_epd.so"
         RPATH "")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/srikanth/revkit-2.2/ext/lib/libcudd_epd.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/srikanth/revkit-2.2/ext/lib" TYPE SHARED_LIBRARY FILES "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/libcudd_epd.so")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_epd.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_epd.so")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_epd.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_nanotrav.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_nanotrav.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_nanotrav.so"
         RPATH "")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/srikanth/revkit-2.2/ext/lib/libcudd_nanotrav.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/srikanth/revkit-2.2/ext/lib" TYPE SHARED_LIBRARY FILES "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/libcudd_nanotrav.so")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_nanotrav.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_nanotrav.so")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_nanotrav.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_obj.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_obj.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_obj.so"
         RPATH "")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/srikanth/revkit-2.2/ext/lib/libcudd_obj.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/home/srikanth/revkit-2.2/ext/lib" TYPE SHARED_LIBRARY FILES "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/libcudd_obj.so")
  IF(EXISTS "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_obj.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_obj.so")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/srikanth/revkit-2.2/ext/lib/libcudd_obj.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/home/srikanth/revkit-2.2/build/ext/cudd-prefix/src/cudd-build/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
