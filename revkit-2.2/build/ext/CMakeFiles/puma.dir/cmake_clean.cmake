FILE(REMOVE_RECURSE
  "CMakeFiles/puma"
  "CMakeFiles/puma-complete"
  "puma-prefix/src/puma-stamp/puma-install"
  "puma-prefix/src/puma-stamp/puma-mkdir"
  "puma-prefix/src/puma-stamp/puma-download"
  "puma-prefix/src/puma-stamp/puma-update"
  "puma-prefix/src/puma-stamp/puma-patch"
  "puma-prefix/src/puma-stamp/puma-configure"
  "puma-prefix/src/puma-stamp/puma-build"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/puma.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
