FILE(REMOVE_RECURSE
  "CMakeFiles/aiger"
  "CMakeFiles/aiger-complete"
  "aiger-prefix/src/aiger-stamp/aiger-install"
  "aiger-prefix/src/aiger-stamp/aiger-mkdir"
  "aiger-prefix/src/aiger-stamp/aiger-download"
  "aiger-prefix/src/aiger-stamp/aiger-update"
  "aiger-prefix/src/aiger-stamp/aiger-patch"
  "aiger-prefix/src/aiger-stamp/aiger-configure"
  "aiger-prefix/src/aiger-stamp/aiger-build"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/aiger.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
