FILE(REMOVE_RECURSE
  "CMakeFiles/cudd"
  "CMakeFiles/cudd-complete"
  "cudd-prefix/src/cudd-stamp/cudd-install"
  "cudd-prefix/src/cudd-stamp/cudd-mkdir"
  "cudd-prefix/src/cudd-stamp/cudd-download"
  "cudd-prefix/src/cudd-stamp/cudd-update"
  "cudd-prefix/src/cudd-stamp/cudd-patch"
  "cudd-prefix/src/cudd-stamp/cudd-configure"
  "cudd-prefix/src/cudd-stamp/cudd-build"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/cudd.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
