message(STATUS "downloading...
     src='http://www.informatik.uni-bremen.de/revkit/files/aiger-1.9.4.tar.gz'
     dst='/home/srikanth/revkit-2.2/build/ext/aiger-1.9.4.tar.gz'
     timeout='none'")

file(DOWNLOAD
  "http://www.informatik.uni-bremen.de/revkit/files/aiger-1.9.4.tar.gz"
  "/home/srikanth/revkit-2.2/build/ext/aiger-1.9.4.tar.gz"
  SHOW_PROGRESS
  # no EXPECTED_MD5
  # no TIMEOUT
  STATUS status
  LOG log)

list(GET status 0 status_code)
list(GET status 1 status_string)

if(NOT status_code EQUAL 0)
  message(FATAL_ERROR "error: downloading 'http://www.informatik.uni-bremen.de/revkit/files/aiger-1.9.4.tar.gz' failed
  status_code: ${status_code}
  status_string: ${status_string}
  log: ${log}
")
endif()

message(STATUS "downloading... done")
