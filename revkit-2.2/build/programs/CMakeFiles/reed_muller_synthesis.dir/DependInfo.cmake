# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/srikanth/revkit-2.2/programs/reversible/reed_muller_synthesis.cpp" "/home/srikanth/revkit-2.2/build/programs/CMakeFiles/reed_muller_synthesis.dir/reversible/reed_muller_synthesis.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "BOOST_RESULT_OF_USE_DECLTYPE"
  "__extern_always_inline=inline"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/srikanth/revkit-2.2/build/src/CMakeFiles/revkit_classical.dir/DependInfo.cmake"
  "/home/srikanth/revkit-2.2/build/src/CMakeFiles/revkit_reversible.dir/DependInfo.cmake"
  "/home/srikanth/revkit-2.2/build/src/CMakeFiles/revkit_core.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../ext/include"
  "../src"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
