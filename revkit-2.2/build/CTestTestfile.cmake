# CMake generated Testfile for 
# Source directory: /home/srikanth/revkit-2.2
# Build directory: /home/srikanth/revkit-2.2/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(ext)
SUBDIRS(addons)
SUBDIRS(src)
SUBDIRS(programs)
SUBDIRS(test)
